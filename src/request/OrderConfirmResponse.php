<?php
/*
 * @copyright 2019-2020 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license proprietary
 * @version 12.11.20 06:09:44
 */

declare(strict_types = 1);
namespace dicr\monoparts\request;

use dicr\monoparts\MonoPartsResponse;

/**
 * Ответ на запрос OrderConfirm.
 */
class OrderConfirmResponse extends MonoPartsResponse
{
    public ?string $orderId = null;

    /** результат выполнения оплаты */
    public ?string $state = null;

    /** уточнение причины состояния */
    public ?string $subState = null;

    /**
     * @inheritDoc
     */
    public function attributeFields() : array
    {
        return array_merge(parent::attributeFields(), [
            'subState' => 'order_sub_state'
        ]);
    }
}
